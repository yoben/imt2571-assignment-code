<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /**
	 * @throws PDOException
     */
    public function __construct($db = null)  
    {  
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
            // Create PDO connection
            $this->db = new PDO('mysql:host=localhost;dbname=test', "root", "");
		}
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
        $booklist = array();
        try {
            $get = $this->db->prepare("SELECT * FROM book");
            $get->execute();
            
            $result = $get->fetchAll(PDO::FETCH_ASSOC);
        
            foreach ($result as $key=>$book) {
                $booklist[$key] = new Book($book['title'], $book['author'], $book['description'], $book['id']);
            }
            return $booklist;
        }
        catch(PDOException $E) {
            echo "Error";
            $E->getMessage();
        } 
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
        try {
            $ret = $this->db->prepare("SELECT * FROM book WHERE id=" . $id);
            $ret->execute();
            $book = null;
            
            $result = $ret->fetch(PDO::FETCH_ASSOC);
            
            if ($result) { 
                $book = new Book($result['title'], $result['author'], $result['description'], $result['id']);
                return $book;
            }
            else {
               return $book; 
            }  
        }
        catch(PDOException $E)  {   
            echo "Error";
            $E->getMessage();
        }
    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {  
        if (isset($book->title) && isset($book->author)) {   
            try  {
                $add = $this->db->prepare("INSERT INTO book (title, author, description) VALUES (:title, :author, :description)");
                $add->bindValue(':title', $book->title, PDO::PARAM_STR);
                $add->bindValue(':author', $book->author, PDO::PARAM_STR);
                $add->bindValue(':description', $book->description, PDO::PARAM_STR); 
           
                if ($book->title != "" && $book->author != "")  {
                    $add->execute();
                    $book->id = $this->db->lastInsertId();
                }
                else {
                    echo "You have to write something under Title and Author";
                    $view = new ErrorView();
                    $view->create();
                }
            }
            catch (PDOException $E) {
                echo "Error";
                $view = new ErrorView();
                $view->create();
                $E->getMessage();
            } 
        }  
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
        $id = $book->id;
        $book->title = $_POST['title'];
        $book->author = $_POST['author'];
        $book->description = $_POST['description'];
        if ($book->title != "" && $book->author != "") { 
            try { 
                $modify = $this->db->prepare("UPDATE book SET title = :title, author = :author, description = :description WHERE id =$id");
        
                // $modify->bindValue(':id', $book->id, PDO::PARAM_INT);
                $modify->bindValue(':title', $book->title, PDO::PARAM_STR);
                $modify->bindValue(':author', $book->author, PDO::PARAM_STR);
                $modify->bindValue(':description', $book->description, PDO::PARAM_STR);
                $modify->execute(); 
            }
            catch (PDOException $E) {
                echo "Error";
                $E->getMessage();
                $view = new ErrorView();
                $view->create();
            }
        }
        else {
            echo "Title and author cannot be empty";
        }
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
        try {
            $delete = $this->db->prepare("DELETE FROM book WHERE id=". $id);
            $delete->bindValue(':id', $id, PDO::PARAM_INT);
            $delete->execute();
        }
        catch (PDOException $E) {
            $E->getMessage();
            $view = new ErrorView();
            $view->create();
        }
    }
}
?>